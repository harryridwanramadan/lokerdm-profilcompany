$(document).ready(function(){
	$("#carousel0").owlCarousel({
		items: 1,
		autoPlay: 3000,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
		pagination: false
	});
	$("#slideshow0").owlCarousel({
		items: 3,
		singleItem: false,
		autoPlay: 3000,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
		pagination: false
	});
});